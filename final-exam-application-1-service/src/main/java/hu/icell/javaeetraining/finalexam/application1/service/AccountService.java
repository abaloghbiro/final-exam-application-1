package hu.icell.javaeetraining.finalexam.application1.service;

import java.util.List;

import hu.icell.javaeetraining.finalexam.application1.dto.AccountDTO;
import hu.icell.javaeetraining.finalexam.application1.dto.CustomerDTO;

public interface AccountService {

	void createAccount(AccountDTO dto);

	void createAccount(AccountDTO acc, CustomerDTO cus);

	List<AccountDTO> getAllAccountsOfASpecificCustomer(Integer customerId);

	void transferMoney(String accountNumberSource, String accountNumberDest, double balance);

	AccountDTO getAccountByAccountNumber(String accountNumber);

	AccountDTO getAccountById(Integer id);

	void deleteAccount(Integer id);

	void deleteAccountByAccNumber(String accNumber);

}
