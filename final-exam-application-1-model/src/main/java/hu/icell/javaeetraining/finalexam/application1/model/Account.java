package hu.icell.javaeetraining.finalexam.application1.model;

public class Account extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accountName;
	private Customer owner;
	private Double balance;
	private String accountNumber;

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Customer getOwner() {
		return owner;
	}

	public void setOwner(Customer owner) {
		this.owner = owner;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

}
